(define (P n z)
	  (define (iter n z)
		(if (zero? z)
			z
			(if (zero? n)
				z
				(iter (- n 1) (- z 1)))))
	  (iter n z))

(define Subtract(lambda (a b)         
		(P b a) ))	

(define T(lambda (a b)         
		 a ))
		 
(define F(lambda (a b)         
		 b ))
		 

(define Ifelse(lambda (c a b)         
		(c a b) ))
		
(define And(lambda (M N)         
		 (N (M T F) F)))
		 
(define (ISZERO n)
	  (if (zero? n)
		T
		F
	  ))
	  
(define Not(lambda (M)         
		 (M F T)))
	  
	  
(define LT(lambda (a b)       
		((And (ISZERO (Subtract a b)) (Not(ISZERO (Subtract b a))))
		T
		F
		)))
		
		
(define Or(lambda (M N)         
		 (N T (M T F))))
		 
(define (S n z)
	  (define (iter n z)
	    (if (zero? n)
	        z
	        (iter (- n 1) (+ z 1))))
	  (iter n z))
	  
(define Add(lambda (a b)         
		(S a (S b 0)) ))
Chat Conversation End
